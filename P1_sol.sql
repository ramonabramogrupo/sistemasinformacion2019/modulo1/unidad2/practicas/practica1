/* Pr�ctica de SQL*/
  USE empleados;
  
/* 1. Mostrar todos los campos y todos los registros de la tabla empleado */
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple;

/*2. Mostrar todos los campos y todos los registros de la tabla departamento */
  SELECT dept_no,
         dnombre,
         loc
    FROM depart;

/*3. Mostrar el apellido y oficio de cada empleado.*/
  SELECT apellido,oficio 
    FROM emple;

/*4. Mostrar localizaci�n y n�mero de cada departamento.*/
  SELECT loc,dept_no
    FROM depart;

/*5. Mostrar el n�mero, nombre y localizaci�n de cada departamento.*/
  SELECT dept_no, dnombre, loc 
    FROM depart;

/*6. Indicar el n�mero de empleados que hay.*/
  SELECT 
    COUNT(*)
    FROM emple;


/*7. Datos de los empleados ordenados por apellido de forma ascendente*/
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY apellido;

/*8. Datos de los empleados ordenados por apellido de forma descendente*/
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY apellido DESC ;

/*9. Indicar el numero de departamentos que hay*/
  SELECT 
    COUNT(*) 
    FROM depart;

/*10. Indicar el n�mero de empleados mas el numero de departamentos*/
  SELECT 
    (SELECT 
      COUNT(*) 
      FROM emple
     )+
    (SELECT 
      COUNT(*) 
      FROM depart
      )suma;

-- 11. Datos de los empleados ordenados por n�mero de departamento descendentemente.
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY dept_no DESC ;

-- 12. Datos de los empleados ordenados por n�mero de departamento descendentemente y por oficio ascendente.
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY dept_no DESC,oficio  ;

-- 13. Datos de los empleados ordenados por n�mero de departamento descendentemente y por apellido ascendentemente.
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    ORDER BY dept_no DESC,apellido ;

-- 14. Mostrar los c�digos de los empleados cuyo salario sea mayor que 2000.
  SELECT emp_no 
    FROM emple 
    WHERE salario>2000;

-- 15. Mostrar los c�digos y apellidos de los empleados cuyo salario sea menor que 2000.
  SELECT emp_no,apellido 
    FROM emple 
    WHERE salario<2000;

-- 16. Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500
    SELECT emp_no,
           apellido,
           oficio,
           dir,
           fecha_alt,
           salario,
           comision,
           dept_no
      FROM emple 
      WHERE salario BETWEEN 1500 AND 2500;

-- 17. Mostrar los datos de los empleados cuyo oficio sea "ANALISTA".
    SELECT emp_no,
           apellido,
           oficio,
           dir,
           fecha_alt,
           salario,
           comision,
           dept_no
      FROM emple 
      WHERE oficio= 'Analista';

-- 18. Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 �
    SELECT emp_no,
           apellido,
           oficio,
           dir,
           fecha_alt,
           salario,
           comision,
           dept_no
      FROM emple 
      WHERE oficio= 'Analista' AND salario>2000;

-- 19. Seleccionar el apellido y oficio de los empleados del departamento n�mero 20.
  SELECT apellido,oficio 
    FROM emple 
    WHERE dept_no=20;
 
-- 20. Contar el n�mero de empleados cuyo oficio sea VENDEDOR
  SELECT 
    COUNT(*) 
    FROM emple 
    WHERE oficio='vendedor';

-- 21. Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma
-- ascendente.
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    WHERE apellido LIKE 'm%' OR apellido LIKE 'n%' 
    ORDER BY apellido;

-- 22. Seleccionar los empleados cuyo oficio sea 'VENDEDOR'. Mostrar los datos ordenados por apellido de forma ascendente.
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    WHERE oficio='vendedor' 
    ORDER BY apellido;
                                                      
-- 23. Mostrar los apellidos del empleado que mas gana.
  SELECT apellido 
    FROM emple 
    WHERE salario=(
      SELECT MAX(salario) 
        FROM emple
      );
  -- Otra opci�n m�s r�pida,si no hay repetidos de salario:
    SELECT * 
      FROM emple
      ORDER BY salario
      LIMIT 1;

-- 24. Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea 'ANALISTA'. Ordenar el resultado por apellido y oficio
-- de forma ascendente.
  SELECT emp_no,
         apellido,
         oficio,
         dir,
         fecha_alt,
         salario,
         comision,
         dept_no
    FROM emple 
    WHERE dept_no=10 AND oficio='Analista' 
    ORDER BY apellido AND oficio;

-- 25. Realizar un listado de los distintos meses en que los empleados se han dado de alta.
  SELECT DISTINCT MONTH(fecha_alt) 
    FROM emple ; -- Si me est� pidiendo el n�mero de mes.

  SELECT DISTINCT MONTHNAME(fecha_alt)
    FROM emple ; -- Si me est� pidiendo el nombre del mes.

-- 26. Realizar un listado de los distintos a�os en que los empleados se han dado de alta 
    SELECT DISTINCT YEAR(fecha_alt)
      FROM emple;

-- 27. Realizar un listado de los distintos d�as del mes en que los empleados se han dado de alta
    SELECT DISTINCT DAYOFMONTH(fecha_alt)
      FROM emple;

-- 28. Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento n�mero 20.
  SELECT apellido 
    FROM emple 
    WHERE salario>2000 OR dept_no=20;

-- 29. Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece.
  SELECT apellido,dnombre 
    FROM 
    depart JOIN emple 
    ON depart.dept_no=emple.dept_no;

-- Otra opci�n m�s �ptima:
  SELECT c1.apellido,dnombre 
    FROM 
      (SELECT apellido,dept_no 
        FROM emple
      ) AS c1
      JOIN depart 
      ON c1.dept_no=depart.dept_no; 

-- 30. Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que
-- pertenece. Ordenar los resultados por apellido de forma descendente.

  SELECT c1.apellido,c1.oficio,dnombre 
    FROM 
      (SELECT apellido,dept_no,oficio 
        FROM emple
      ) AS c1
      JOIN depart 
      ON c1.dept_no=depart.dept_no
    GROUP BY c1.apellido DESC;

-- 31. Listar el n�mero de empleados por departamento.
  SELECT dept_no,COUNT(emp_no)AS 'N�mero_de_empleados' 
    FROM emple 
    GROUP BY dept_no;

-- 32. Realizar el mismo comando anterior pero obteniendo una salida como la que vemos.
  SELECT dnombre,c2.N�mero_de_empleados 
    FROM 
    (SELECT dept_no,COUNT(emp_no)AS 'N�mero_de_empleados' 
      FROM emple 
      GROUP BY dept_no
     ) AS c2
    JOIN depart
    ON c2.dept_no=depart.dept_no ;


-- 33. Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre.
  SELECT apellido
    FROM emple
    ORDER BY oficio, apellido;

-- 34. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por 'A'. Listar el apellido de los empleados.
  SELECT apellido
    FROM emple
    WHERE apellido LIKE 'A%';

-- 35. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ?A' o por �M�. Listar el apellido de los empleados.
  SELECT apellido
    FROM emple
    WHERE apellido LIKE 'A%' OR apellido LIKE 'M%' ;
   
-- 36. Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ?Z'. Listar todos los campos de la tabla empleados.
  SELECT *   
    FROM emple
    WHERE apellido NOT LIKE '%Z';

-- 37. Seleccionar de la tabla EMPLE aquellas filas cuyo APELLIDO empiece por ?A' y el OFICIO tenga una ?E' en cualquier
-- posici�n. Ordenar la salida por oficio y por salario de forma descendente.
  SELECT *
    FROM emple
      WHERE 
      apellido LIKE 'A%'
      AND 
      oficio LIKE '%E%';

